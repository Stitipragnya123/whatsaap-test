package SeleniumPractice;
import java.time.Duration;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class WpChatBot {
	//public static void main(String[] args) {
	public void Chat(){
	
		GregorianCalendar cal = new GregorianCalendar();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");  
		 WebDriver driver = new ChromeDriver();
		 
		 // WebDriverWait wait=new WebDriverWait(WebDriveReference,TimeOut);
		 WebDriverWait wait=new WebDriverWait(driver,120);
		 driver.navigate().to("https://web.whatsapp.com/");
		 driver.manage().window().maximize();
		 WebElement ele;
		    ele = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"side\"]/div[1]/div/label/div/div[2]")));
		    ele.sendKeys("Debojoyti Whatsapp");
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//span/span[text()='Debojoyti Whatsapp']")).click();
			WebElement textBox;
			textBox = driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]"));
	        if (hour<12){
	        	textBox.sendKeys("Good Morning!!!");
	        }
	        else if (hour>12 && hour<17){
	        	textBox.sendKeys("Good Afternoon!!!");
	        }
	        else {
	        	textBox.sendKeys("Good Evening!!!");
	        }
	        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Here is an meeting  for you.");
	        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]")).click();
	        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Please confirm to Reply 1 or Reply 2");
	        driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]")).click();
	    	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	        WebElement reply=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")));
	        String i=new String(reply.getText());
	        if (i.equals("1")){

	        	textBox.sendKeys("Meeting will be sheduled");
	        }

	        if(i.equals("2")){

	        	textBox.sendKeys("Meeting is canceled");
	        }

	        

		 }

}
}
